var switched = false;

Template.kostymer.events({
    'mouseover .kostymer .model': function(event){
        if(!switched){
            var swatchObj = $($(event.currentTarget).parent().children()[1]);

            swatchObj.css("top", "100px").css("top", "100px").fadeIn();
            swatchObj.offset($(event.currentTarget).offset());
        }
    },
    'mouseout .kostymer .swatch': function(event){
        if(!switched) $(event.currentTarget).fadeOut();
        else if($(event)[0].originalEvent.explicitOriginalTarget.className !== "model") $(event.currentTarget).fadeIn();
    },
    'mouseover .kostymer .swatch': function(event){
        if(switched) $(event.currentTarget).fadeOut();
    },
    'mouseout .kostymer .model': function(event){
        if(switched) $($(event.currentTarget).parent().children()[1]).fadeIn();
    },
    'click span.onoffswitch-inner': function(){
        if(!switched){
            _.each($('.kostymer .row .swatch'), function(kostym){
                var swatchObj = $(kostym);
                swatchObj.css("top", "100px").css("top", "100px").fadeIn();
                swatchObj.offset($(swatchObj.parent().children()[0]).offset());
            });
            switched = true;
        } else {
            _.each($('.kostymer .row .swatch'), function(kostym){
                $(kostym).fadeOut();
            });
            switched = false;
        }
    }
});

$(document).ready(function(){
    $('body').hover(function(){
        if($('.kostymer')[0]){
            //TODO: Get better functioning for switching between swatch and costumes
        }
    });
});