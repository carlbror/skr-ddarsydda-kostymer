var standardTitle = "Internets herrekipering",
    standardUrl = "http://213.112.213.170:81";

Blaze._allowJavascriptUrls();

metaController = RouteController.extend({
    metaProperties: {
        'og:title': function(){
            return standardTitle + " | " + capitaliseFirstLetter(this.route._path.slice(1))
        },
        'og:url': function(){
            return standardUrl + this.route._path
        },
        'og:image': "http://213.112.213.170:81/bike-suit.jpg"
    }
});

Router.configure({
    layoutTemplate: 'layout', waitOn: function(){
    }
});


Router.map(function(){
    this.route('framsida', {path: '/'});

    this.route('kostymer', {
        path: '/kostymer/:gender',
        controller: "metaController",
        data: function(){
            return {suits: Articles.find({type:"suit", gender: this.params.gender}).fetch()};
        }
    });

    this.route('kostymSida', {
        path: '/kostym/:url',
        waitOn: function(){
            return Meteor.subscribe("articles");
        },
        data: function(){
            return {suit: Articles.findOne({url: this.params.url})}
        }
    });

    this.route('kassan', {
        waitOn: function(){
            return Meteor.subscribe('orders') && Meteor.subscribe("articles");
        },
        data: function(){
            return {orders: Orders.find().fetch()}
        }
    });

    this.route('storlek', {
        path: '/storlek'
    });
});


capitaliseFirstLetter = function(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
};