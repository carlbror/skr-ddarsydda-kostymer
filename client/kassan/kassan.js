var cost = 0,
    shipping = 0;

Template.kassan.helpers({
    my: function(){
        console.log(this);
    },
    suitFromSuitId: function(){
        return Articles.findOne(this.articleId);
    },
    costWithoutShipping: function(){
        _.each(this.orders, function(order){
            cost = cost + Articles.findOne(order.articleId).price;
        });

        return cost;
    },
    shippingCost: function(){
        if(cost){
            if(cost < 1000){
                shipping = 300;
                return 300;
            } else {
                shipping = 0;
                return "Gratis";
            }
        }
    },
    totalCost: function(){
        return cost + shipping;
    }
});

Template.kassan.events({
    'click .orders .float-left-tenth:first-child i': function(){
        Meteor.call('removeOrder', this._id, function(err){
            if(err) console.log("Error! " + err.message);
        });
    },
    'click .orders .change-design': function(event){
        Box.render(this);
    }
});