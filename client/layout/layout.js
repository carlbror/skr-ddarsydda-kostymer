var timeToSlideMenu = 500;
var menuOpen = false;

Template.layout.events({
    'click .icon-menu': function(){
        $('.menu').animate({
            left: "0px"
        }, timeToSlideMenu);

        $('body').animate({
            left: "285px"
        }, timeToSlideMenu);

        menuOpen = true;
    },
    'click .icon-close': function(){
        $('.menu').animate({
            left: "-285px"
        }, timeToSlideMenu);

        $('body').animate({
            left: "0px"
        }, timeToSlideMenu);

        menuOpen = false;
    },
    'click .menu ul a': function(){
        $('.menu').animate({
            left: "-285px"
        }, timeToSlideMenu);

        $('body').animate({
            left: "0px"
        }, timeToSlideMenu);

        menuOpen = false;
    }
});

Template.layout.rendered = function(){
    $(document).ready(function(){
        $('body').click(function(event){
            var target = $(event.target);
            if(target.attr('class') !== 'icon-menu' && target.attr('class') !== 'menu' && !target.parents('.menu').length){
                $('.menu').animate({
                    left: "-285px"
                }, timeToSlideMenu);

                $('body').animate({
                    left: "0px"
                }, timeToSlideMenu);
            }
        });
    });
};