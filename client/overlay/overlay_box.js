var oldOrder;

function overlayBox(){
    this.render = function(order){
        var windowWidth = window.innerWidth;
        var windowHeight = window.innerHeight;
        var dialogoverlay = document.getElementById('dialogoverlay');
        var dialogbox = document.getElementById('dialogbox');

        dialogoverlay.style.display = "block";
        dialogoverlay.style.height = windowHeight + "px";

        if(windowWidth > 900){
            dialogbox.style.left = (windowWidth / 2) - (900 * 0.5) + "px";
        } else {
            dialogbox.style.width = "100%";
        }

        $('#dialogoverlay').fadeIn(250,function(){
            $(dialogbox).fadeIn(250);
            $('html,body').animate({scrollTop: $("#dialogboxhead").offset().top}, 250);

            if(order && order.customization){
                oldOrder = order;
                _.each(order.customization, function(custom){
                    var chosenCustomizationClass = custom.replace(" ", "-").toLowerCase();
                    $('#dialogboxbody .' + chosenCustomizationClass).parent().parent().children().children('p').css({"background-color": "#404040"}).children().hide();
                    $('#dialogboxbody .' + chosenCustomizationClass).css({"background-color": "#29e260"}).children().show()
                });
            } else {
                $('#dialogboxbody .1 .col-xs-6:first-child p').css({"background-color": "#29e260"}).children().show();
                $('#dialogboxbody .2 .col-xs-6:first-child p').css({"background-color": "#29e260"}).children().show();
            }
            $('.1').show().siblings().hide();
        });

        $('body').css({'overflow': 'hidden'});
    }
    this.close = function(){
        $('#dialogbox').fadeOut(250, function(){
            $('#dialogoverlay').fadeOut(250);
            var currentTab = parseInt($('.active').attr('class').substring(0.1));

            if($('.finish')[0]) $('.next').removeClass('finish').html('Nästa');
            if(currentTab !== 1) $('.previous').hide();
            if(currentTab !== 1) {
                $("." + currentTab).removeClass('active');
                $(".1").addClass('active').show().siblings().hide();
            }
            $('body').css({'overflow': 'visible'});
        });
    }
};
Box = new overlayBox();

Template.overlayBox.events({
    'click .start-custom-alert': function(){
        Box.render("no truth");
    },
    'click .close-custom-alert': function(){
        Box.close();
    },
    'click .next': function(event){
        var currentTab = parseInt($('.active').attr('class').substring(0.1)),
            lastTab = $('.tab-content')[0].children.length;

        if(currentTab === lastTab){
            var customizedArray = [];
            $('#dialogboxbody .col-sm-2 p').each(function(){
                if(this.style && this.style.backgroundColor === "rgb(41, 226, 96)"){
                    for(x=0;x < this.innerHTML.length; x++){
                        if(this.innerHTML.charAt(x) === "<"){
                            customizedArray.push(this.innerHTML.substring(0, x).trim());
                            break;
                        }
                    }
                }
            });

            if(oldOrder){
                Meteor.call('changeOrder', oldOrder._id, customizedArray, function(err){
                    Box.close();
                });
            } else {
                Meteor.call('addOrder', this._id, customizedArray, function(err, orderId){//
                    $('body').css({'overflow': 'visible'});
                    Router.go('kassan');
                });
            }
        } else{
            $("." + (currentTab +1)).addClass('active').show().siblings().hide();
            $("." + currentTab).removeClass('active');
            if(currentTab === 1) $('.previous').show();
            if(currentTab === lastTab-1){
                $('.next').addClass('finish').html('Klar!');
            }
        }
    },
    'click .previous': function(event){
        var currentTab = parseInt($('.active').attr('class').substring(0.1)),
            lastTab = $('.tab-content')[0].children.length;

        $("." + (currentTab - 1)).addClass('active').show().siblings().hide();
        $("." + currentTab).removeClass('active');
        if(currentTab === 2) $('.previous').hide();
        if(currentTab === lastTab) $('.next').removeClass('finish').html('Nästa');
    },
    'click #dialogboxbody .row p': function(event){
        $("#dialogboxbody ." + parseInt($(event.target).parent().parent().parent().attr('class').substring(0.1))
        + " .col-xs-6 p").css({"background-color": "#404040"}).children().hide();
        $(event.target).css({"background-color": "#29e260"}).children().show();
    },
    'click #dialogboxbody .row img': function(event){
        $("#dialogboxbody ." + parseInt($(event.target).parent().parent().parent().attr('class').substring(0.1))
            + " .col-xs-6 p").css({"background-color": "#404040"}).children().hide();
        $(event.target).siblings().css({"background-color": "#29e260"}).children().show();
    }
});

Template.overlayBox.rendered = function(){
    $('body').on('keydown', function(e){
        if(e.which === 27){
            Box.close();
        }
    });

    $('body').on('click', function(e){
        if(e.target.id === "dialogoverlay"){
            Box.close();
        }
    });

    $(window).resize(function(){
        var windowWidth = window.innerWidth;

        if(windowWidth > 900){
            dialogbox.style.width = "900px";
            dialogbox.style.left = (windowWidth / 2) - (900 * 0.5) + "px";
        } else {
            dialogbox.style.width = "100%";
            dialogbox.style.left = 0;
        }

        document.getElementById('dialogoverlay').style.height = window.innerHeight + "px";
    });
};