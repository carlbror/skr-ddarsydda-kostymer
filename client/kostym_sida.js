Template.kostymSida.helpers({
    'dateInThirtyDays': function(){
        return moment(new Date()).add(30, 'days').calendar();
    }
});

Template.kostymSida.events({
    'click .read-more .button': function(event){
        var totalHeight = 0,
            eventTarget = $(event.target);

        var div = eventTarget.parent().parent().children('.kostym-beskrivning');
        var paragraphs = div.find("p:not('.read-more')");

        paragraphs.each(function(){
            totalHeight += $(this).outerHeight();
        });

        div.css({
            "height": div.height(),
            "max-height": 9999
        })
            .animate({
                "height": totalHeight
            });

        $(eventTarget.parent()).switchClass('read-more', "read-less");
        eventTarget.html('Stäng');

        return false;
    },
    'click .read-less .button': function(event){
        var eventTarget = $(event.target);
        $(eventTarget.parent().parent().children('.kostym-beskrivning')).animate({"height": 60, "max-height": 60});
        $(eventTarget.parent()).switchClass('read-less', "read-more");
        eventTarget.html('Läs mer');

        return false;
    }
});

Template.kostymSida.rendered = function(){
    $('.kostym-sida .col-sm-7').ready(function(){
        $('.kostym-sida .suit-large-background').css('background-image', "url(" + Template.instance().data.suit.modelSrc + ")");
    });
};

callSuitOverlay = function(){
    Box.render();
};