Template.framsida.rendered = function(){
    $(".owl-jumbotron").owlCarousel({
        dragEndSpeed: 1100,
        autoplaySpeed: 1100,
        fluidSpeed: 1500,
        items:1,
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true
    });
};

Template.framsida.events({
    'mouseover .some-pictures .col-sm-4': function(event){
       $(event.currentTarget).children('.image-container').children('a').children('.wrapper').children('img').stop(true, false).animate({transform: 'scale(1.04)'});
    },
    'mouseout .some-pictures .col-sm-4': function(event){
       $(event.currentTarget).children('.image-container').children('a').children('.wrapper').children('img').stop(true, false).animate({transform: 'scale(1)'});
    }
});