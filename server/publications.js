Meteor.publish('articles', function(){
    return Articles.find();
});

Meteor.publish('orders', function(userId){
    if(userId){
        return Orders.find({customerId: userId});
    } else if(this.connection.clientAddress || this.connection.httpHeaders){
        return Orders.find({unregisteredCustomerIpOrHttpHeaders: this.connection.clientAddress || this.connection.httpHeaders});
    }
});