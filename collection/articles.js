Articles = new Meteor.Collection("articles");

Meteor.methods({
    addSuit: function(name, price, material, gender){
        var urlName = name.toLowerCase().trim().replace(' ', "-");
        if(!gender){gender = "dam"}
        Articles.insert({
            type: "suit",
            gender: gender,
            name: name,
            material: material,
            url: urlName,
            modelSrc: '/suits/' + gender + '/' + urlName + '-model.jpg',
            swatchSrc: '/suits/' + gender + '/' + urlName + '-swatch.jpg',
            miniSrc: '/suits/' + gender + '/' + urlName  + '-mini.jpg',
            price: price
        });
    }
});