Meteor.methods({
    getCustomerIpOrHttpHeaders: function(){
        if(Meteor.isServer) return this.connection.clientAddress || this.connection.httpHeaders
    }
});