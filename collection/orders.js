Orders = new Meteor.Collection('orders');

Orders.allow({
    insert: function(something){
        console.log(something);
        console.log(this);
        return true
    }
});

Meteor.methods({
    'addOrder': function(articleId, customization){
        if(!Meteor.user()){
            if(Meteor.isServer){
                if(this.connection.clientAddress){
                    return Orders.insert({
                        unregisteredCustomerIpOrHttpHeaders: this.connection.clientAddress || this.connection.httpHeaders,
                        articleId: articleId,
                        customization: customization,
                        date: new Date()
                    });
                }
            }
        } else {
            return Orders.insert({
                customerId: Meteor.userId(),
                articleId: articleId,
                customization: customization,
                date: new Date()
            });
        }
    },
    removeOrder: function(orderId){
        Orders.remove({_id: orderId});
    },
    changeOrder: function(orderId, customization){
        Orders.update({_id: orderId}, {$set: {customization: customization}});
    }
});

// TODO: When an unregistered customer becomes a user, their orders need to be changed the moment these users become real